#####################################################################################################################################################
######################################################################## INFO #######################################################################
#####################################################################################################################################################

"""
    This script makes multiple games between two programs, and compares the obtained scores.
    It performs two analyses: a quick average analysis and a formal 1 sample T test.
"""

#####################################################################################################################################################
###################################################################### IMPORTS ######################################################################
#####################################################################################################################################################

# Import PyRat
from pyrat import *

# External imports
import matplotlib.pyplot as pyplot
import scipy.stats
import numpy
import types
import tqdm

# Previously developed functions
import greedy 

# Create aliases for the functions you need
program_1_preprocessing = greedy.preprocessing if "preprocessing" in dir(greedy) else None
program_1_turn = greedy.turn
program_1_postprocessing = greedy.postprocessing if "postprocessing" in dir(greedy) else None

program_2_preprocessing = greedy.preprocessing if "preprocessing" in dir(greedy) else None
program_2_turn = greedy.turn
program_2_postprocessing = greedy.postprocessing if "postprocessing" in dir(greedy) else None

#####################################################################################################################################################
############################################################### VARIABLES & CONSTANTS ###############################################################
#####################################################################################################################################################

"""
    Number of games to make.
"""

NB_GAMES = 10

#####################################################################################################################################################

"""
    Games configuration.
"""

MAZE_WIDTH = 21
MAZE_HEIGHT = 15
MUD_PERCENTAGE = 0.0
NB_CHEESE = 40
TURN_TIME = 0.0
PREPROCESSING_TIME = 0.0
SYNCHRONOUS = True

#####################################################################################################################################################
##################################################################### FUNCTIONS #####################################################################
#####################################################################################################################################################

def run_one_game(seed: int,
                 preprocessing_1, turn_1, postprocessing_1,
                 preprocessing_2, turn_2, postprocessing_2) -> Dict[str, Any]:

    """
        This function runs a PyRat game, with no GUI, for a given seed and program, and returns the obtained stats.
        In:
            * seed:      Random seed used to create the game.
            * program_1: First program to use in that game.
            * program_2: Second program to use in that game.
        Out:
            * stats: Statistics output at the end of the game.
    """

    # Map the functions to the character
    players = [{"name": "greedy_1",
                "team": "1",
                "preprocessing_function": preprocessing_1,
                "turn_function": turn_1,
                "postprocessing_function": postprocessing_1},
               {"name": "greedy_2",
                "team": "2",
                "preprocessing_function": preprocessing_2,
                "turn_function": turn_2,
                "postprocessing_function": postprocessing_2}]

    # Customize the game elements
    config = {"maze_width": MAZE_WIDTH,
              "maze_height": MAZE_HEIGHT,
              "mud_percentage": MUD_PERCENTAGE,
              "nb_cheese": NB_CHEESE,
              "render_mode": "no_rendering",
              "preprocessing_time": PREPROCESSING_TIME,
              "turn_time": TURN_TIME,
              "synchronous": True,
              "random_seed": seed}
        
    # Start the game
    game = PyRat(players, **config)
    stats = game.start()
    return stats
    
#####################################################################################################################################################
######################################################################## GO! ########################################################################
#####################################################################################################################################################

if __name__ == "__main__":

    # Run multiple games
    results = []
    for seed in tqdm.tqdm(range(NB_GAMES), desc="Game", position=0, leave=False):
        
        # Store score difference as result
        stats = run_one_game(seed, 
                             program_1_preprocessing, program_1_turn, program_1_postprocessing,
                             program_2_preprocessing, program_2_turn, program_2_postprocessing)
        
        
        results.append(int(stats["players"]["greedy_1"]["score"] - stats["players"]["greedy_2"]["score"]))
        
    # Show results briefly
    print("#" * 20)
    print("#  Quick analysis  #")
    print("#" * 20)
    rat_victories = [score for score in results if score > 0]
    python_victories = [score for score in results if score < 0]
    nb_draws = NB_GAMES - len(rat_victories) - len(python_victories)
    print("greedy_1", "(rat)   <-  ", len(rat_victories), "  -  ", nb_draws, "  -  ", len(python_victories), "  ->  ", "greedy_2", "(python)")
    print("Average score difference when greedy_1 wins:", numpy.mean(rat_victories) if len(rat_victories) > 0 else "n/a")
    print("Average score difference when greedy_2 wins:", numpy.mean(numpy.abs(python_victories)) if len(python_victories) > 0 else "n/a")


    # More formal statistics to check if the mean of the distribution is significantly different from 0
    print("#" * 21)
    print("#  Formal analysis  #")
    print("#" * 21)
    test_result = scipy.stats.ttest_1samp(results, popmean=0.0)
    print("One sample T-test of the distribution:", test_result)

    # Visualization of histograms of score differences
    bins = range(min(results), max(results) + 2)
    pyplot.figure(figsize=(20, 10))
    pyplot.hist(results, ec="black", bins=bins)
    pyplot.title("Analysis of the game results in terms of victory margin")
    pyplot.xlabel("score(greedy_1) - score(greedy_2)")
    pyplot.xticks([b + 0.5 for b in bins], labels=bins)
    pyplot.xlim(bins[0], bins[-1])
    pyplot.ylabel("Number of games")
    pyplot.show()
    
#####################################################################################################################################################
#####################################################################################################################################################