#######################################################################################################################################
############################################################# INFO ####################################################################
#######################################################################################################################################

"""
Mariem MARZOUGUI Antoine DAGORN - AI Challenge
"""



#####################################################################################################################################################
######################################################################## INFO #######################################################################
#####################################################################################################################################################

"""
    This program aims at training the rat to beat a greedy opponent.
    To do so, we use reinforcement learning, and more precisely deep Q learning (DQN).
    If you set the TRAIN_MODEL constant to True, the rat will be training.
    Otherwise it will just try to get the cheese using what it has learned.

    In this lab, we will work on a full maze with no mud.
    The settings have been set to a 10x10 maze with 15 pieces of cheese.
    You can change these settings if you want.

    This file already provides you with a functional RL algorithm.
    However, it is not very efficient, and you will have to improve it.
    Before you do so, you should understand how it works.

    The file makes use of Weights and Biases (WandB) to monitor the training.
    You can disable its use by setting the USE_WANDB constant to False.
    If you want to use it, you will have to create an account on https://wandb.ai/ and get an API key.
    Then, you will have to store that key in a file named "wandb.key" and set the WANDB_KEY_PATH variable accordingly (by defaultit is set to the same directory as this file).

    Here is your mission:
        1 - Have a look at the code and make sure you understand it.
            Also, make sure the code runs for training (TRAIN_MODEL=True) and playing (TRAIN_MODEL=False).
            If not, you may have to install some packages or change some paths.
        2 - Improve the RL algorithm.
            There are multiple ways to do so, and you are free to choose the ones you want.
            Some ideas:
                * Improve the data representation.
                * Change the reward function.
                * Improve the exploration strategy (some keywords you may be looking for are "epsilon greedy" or "softmax policy" for instance).
                * Change the hyperparameters of the algorithm (discount factor, batch size, etc.).
                * Improve the model architecture.
                * Change the optimizer or its parameters.
                * Change the loss function.
                * Etc.
"""

#####################################################################################################################################################
###################################################################### IMPORTS ######################################################################
#####################################################################################################################################################

# Import PyRat
from pyrat import *

# External imports 
import torch
import random
import math
import numpy
import shutil
import tqdm
import wandb
import sys
import os

# Previously developed functions
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), "..", "lab4"))
import greedy as opponent
from utils import get_opponent_name

#####################################################################################################################################################
############################################################### CONSTANTS & VARIABLES ###############################################################
#####################################################################################################################################################

"""
    Paths where to store the stuff that must be shared from a game to the other.
"""

OUTPUT_DIRECTORY = os.path.join(os.path.dirname(os.path.realpath(__file__)), "RL")
MODEL_FILE_NAME = os.path.join(OUTPUT_DIRECTORY, "ERL_model.pt")
OPTIMIZER_FILE_NAME = os.path.join(OUTPUT_DIRECTORY, "ERL_optimizer.pt")
EXPERIENCE_FILE_NAME = os.path.join(OUTPUT_DIRECTORY, "ERL_experience.pt")

#####################################################################################################################################################

"""
    Indicates if we are in train or test mode.
"""

TRAIN_MODEL = False 
RESET_TRAINING = True

EPSILON_START = 1.0  # Epsilon initial
EPSILON_END = 0.1    # Epsilon final
EPSILON_DECAY = 200  # Nombre de pas pour la décroissance
steps_done = 0

#####################################################################################################################################################

"""
    RL algorithm parameters.
"""

MAX_EXPERIENCE_SIZE = 100
EXPERIENCE_MIN_SIZE_FOR_TRAINING = 1000
NB_BATCHES = 16
BATCH_SIZE = 32
DISCOUNT_FACTOR = 0.9

#####################################################################################################################################################

"""
    Parameters of the optimizer used to train the model.
"""

LEARNING_RATE = 0.1

#####################################################################################################################################################

"""
    Number of PyRat games from which to learn.
"""

NB_EPISODES = 1000

#####################################################################################################################################################

"""
    Wandb parameters.
"""

USE_WANDB = False
WANDB_KEY_PATH = os.path.join(os.path.dirname(os.path.realpath(__file__)), "wandb.key")

#####################################################################################################################################################
############################################################### DEEP Q LEARNING MODEL ###############################################################
#####################################################################################################################################################

class DQN (torch.nn.Module):

    #############################################################################################################################################
    #                                                                CONSTRUCTOR                                                                #
    #############################################################################################################################################

    def __init__(self, h, w, outputs):
        super(DQN, self).__init__()
        self.conv1 = torch.nn.Conv2d(3, 16, kernel_size=3, stride=1, padding=1)
        self.bn1 = torch.nn.BatchNorm2d(16)
        self.conv2 = torch.nn.Conv2d(16, 32, kernel_size=3, stride=1, padding=1)
        self.bn2 = torch.nn.BatchNorm2d(32)
        self.conv3 = torch.nn.Conv2d(32, 64, kernel_size=3, stride=1, padding=1)
        self.bn3 = torch.nn.BatchNorm2d(64)

        """
            This function is the constructor of the class.
            In:
                * self:              Reference to the current object.
                * data_shape:        Shape of the input data.
                * actions_dimension: Number of possible actions.
            Out:
                * self: Reference to the current object.
        """

       # Taille de sortie après les couches convolutives
        def conv2d_size_out(size, kernel_size=3, stride=1, padding=1):
            return (size + 2 * padding - (kernel_size - 1) - 1) // stride + 1
        
        convw = conv2d_size_out(conv2d_size_out(conv2d_size_out(w)))
        convh = conv2d_size_out(conv2d_size_out(conv2d_size_out(h)))
        linear_input_size = convw * convh * 64

        self.head = torch.nn.Linear(linear_input_size, outputs)
                  
    #############################################################################################################################################
    #                                                               PUBLIC METHODS                                                              #
    #############################################################################################################################################

    def forward(self, x):
        x = torch.nn.functional.relu(self.bn1(self.conv1(x)))
        x = torch.nn.functional.relu(self.bn2(self.conv2(x)))
        x = torch.nn.functional.relu(self.bn3(self.conv3(x)))
        return self.head(x.view(x.size(0), -1))

        """
            This function performs a forward pass of the data through the model.
            In:
                * self: Reference to the current object.
                * x:    Input data.
            Out:
                * y: Output data.
        """

#####################################################################################################################################################
##################################################################### FUNCTIONS #####################################################################
#####################################################################################################################################################

def update_target_network(main_network, target_network):
    target_network.load_state_dict(main_network.state_dict())

def build_state(maze, maze_width, maze_height, name, teams, player_locations, cheese):
    """
    Construit un tenseur représentant l'état du jeu pour un CNN.
    """

    # Initialisation d'un tenseur pour représenter l'état du jeu
    # Par exemple, un tenseur 3D avec des canaux pour différents éléments du jeu
    state = torch.zeros((3, maze_height, maze_width), dtype=torch.float)

    # Canal 1 : Position du joueur
    player_pos = player_locations[name]
    state[0, player_pos // maze_width, player_pos % maze_width] = 1

    # Canal 2 : Position de l'adversaire
    opponent_name = get_opponent_name(teams, name)
    opponent_pos = player_locations[opponent_name]
    state[1, opponent_pos // maze_width, opponent_pos % maze_width] = 1

    # Canal 3 : Position des fromages
    for c in cheese:
        state[2, c // maze_width, c % maze_width] = 1

    return state

#####################################################################################################################################################

"""
        This function returns one of the possible actions, given the state of the game.
        In:
            * state:              State of the game in which we are now.
            * possible_actions:   Actions that the agent can perform.
            * model:              Deep Q learning model used to predict the next action.
        Out:
            * action: One action chosen from possible_actions.
    """


def select_action(state, possible_actions, model):
    global EPSILON_START, EPSILON_END, EPSILON_DECAY, steps_done

    # Calcul de la valeur actuelle d'epsilon
    epsilon = EPSILON_END + (EPSILON_START - EPSILON_END) * math.exp(-1. * steps_done / EPSILON_DECAY)

    # Choix entre exploration et exploitation
    if random.random() > epsilon:
        # Exploitation : choisir la meilleure action selon le modèle
        with torch.no_grad():
            return possible_actions[torch.argmax(model(state.unsqueeze(0))).item()]
    else:
        # Exploration : choisir une action aléatoire
        return random.choice(possible_actions)
#####################################################################################################################################################

def compute_reward ( state:                torch.tensor,
                     scores_at_state:      Dict[str, float],
                     new_state:            torch.tensor,
                     scores_at_new_state:  Dict[str, float],
                     name:                 str,
                     teams:                Dict[str, List[str]],
                     initial_cheese_count: int
                   ) ->                    float:
    reward = 0.0
    opponent_name = get_opponent_name(teams, name)

    # Conditions de récompense et pénalité existantes

    #Aucune récompense ou pénalité si les scores des deux joueurs restent inchangés.
    if scores_at_state[name] == scores_at_new_state[name] and scores_at_state[opponent_name] == scores_at_new_state[opponent_name]:
        reward += 0.0

    #Aucune récompense ou pénalité si le joueur a rattrapé le score de son adversaire mais plus de la moitié des fromages ont déjà été collectés.
    if scores_at_new_state[name] == scores_at_state[opponent_name] and scores_at_new_state[name] + scores_at_new_state[opponent_name] >= initial_cheese_count / 2:
        reward += 0.0

    #Récompense positive si le joueur a rattrapé le score de son adversaire et moins de la moitié des fromages ont été collectés.
    if scores_at_new_state[name] == scores_at_state[opponent_name] and scores_at_new_state[name] + scores_at_new_state[opponent_name] < initial_cheese_count / 2:
        reward += 1.0

    #Pénalité si l'adversaire a collecté plus de la moitié des fromages.
    if scores_at_new_state[opponent_name] > initial_cheese_count / 2:
        reward -= 1.0

    #Récompense si le joueur a collecté plus de la moitié des fromages.
    if scores_at_new_state[name] > initial_cheese_count / 2:
        reward += 1.0

    #Récompense significative pour l'augmentation du score du joueur.
    if scores_at_new_state[name] > scores_at_state[name]:
        reward += 2.0

    # Nouvelles conditions de récompense et pénalité
    #closest_cheese_distance = min([distance(player_locations[name], cheese_location, maze_width) for cheese_location in cheese_locations])
    #new_closest_cheese_distance = min([distance(player_locations[name], cheese_location, maze_width) for cheese_location in cheese_locations])
    
    #Récompense pour se rapprocher du fromage le plus proche.
    #if new_closest_cheese_distance < closest_cheese_distance:
    #    reward += 0.5

    #Petite pénalité si aucun fromage n'est collecté et si le joueur s'éloigne du fromage le plus proche.
    #if scores_at_new_state[name] == scores_at_state[name] and new_closest_cheese_distance >= closest_cheese_distance:
     #   reward -= 0.1

    #player_pos = player_locations[name]
    #opponent_pos = player_locations[opponent_name]

    #Récompense pour bloquer l'adversaire (être à une case de distance).
    #if is_blocking_opponent(player_pos, opponent_pos, maze_width):
     #   reward += 0.5

    #Pénalité pour se trouver sur la même case que l'adversaire.
   # if old_player_locations[name] == new_player_locations[name]:
    #    reward -= 1 

    #Pénalité pour être trop près de l'adversaire (à moins de 2 cases de distance).
    #if distance(player_locations[name], player_locations[opponent_name], maze) < 2:
     #   reward -= 0.7


    #if old_player_locations[name] == new_player_locations[name]:
    #    reward -= 1  # Ajustez cette valeur selon le niveau de pénalité souhaité

    return reward

#####################################################################################################################################################

def distance(pos1, pos2, maze_width):
    assert isinstance(pos1, int), "pos1 doit être un entier"
    assert isinstance(pos2, int), "pos2 doit être un entier"
    assert isinstance(maze_width, int), "maze_width doit être un entier"
    y1, x1 = divmod(pos1, maze_width)
    y2, x2 = divmod(pos2, maze_width)
    return abs(x1 - x2) + abs(y1 - y2)



#####################################################################################################################################################

def is_blocking_opponent(player_pos, opponent_pos, maze_width):
    """
    Vérifie si le joueur bloque l'adversaire.
    """
    # Bloquer signifie être à une distance de 1 cellule de l'adversaire
    return distance(player_pos, opponent_pos, maze_width) == 1


#####################################################################################################################################################

def make_batch ( model:            DQN,
                 experience:       List[Dict[str, Any]],
                 possible_actions: List[str]
               ) ->                Tuple[torch.tensor, torch.tensor]:

    """
        This function builds batches from the memory to train the model on.
        Each batch is a pair (data, target), where each element has batch size as first dimension.
        In:
            * model:            Model to train.
            * experience:       List of experience situations encountered across games.
            * possible_actions: Actions mapped with the output of the model.    
        Out:
            * data:    Batch of data from the memory.
            * targets: Targets associated with the sampled data.
    """

    # Get indices
    batch_size = min(BATCH_SIZE, len(experience))
    indices = random.sample(range(len(experience)), batch_size)

    # Create the batch
    data = torch.zeros(batch_size, *experience[0]["state"].shape)
    targets = torch.zeros(batch_size, len(possible_actions))
    for i in range(batch_size):
        
        # Data is the sampled state
        data[i] = experience[indices[i]]["state"]
        
        # Target is the discounted reward
        with torch.no_grad():
            targets[i] = model(data[i].unsqueeze(0))
            if experience[indices[i]]["over"]:
                targets[i, possible_actions.index(experience[indices[i]]["action"])] = experience[indices[i]]["reward"]
            else:
                model_outputs = model(experience[indices[i]]["new_state"].unsqueeze(0))
                targets[i, possible_actions.index(experience[indices[i]]["action"])] = experience[indices[i]]["reward"] + DISCOUNT_FACTOR * torch.max(model_outputs).item()

    # Done
    return data, targets
    
#####################################################################################################################################################

def train_model(main_network, target_network, optimizer, experience, possible_actions):
    """
    This function trains the model on the experience.
    In:
        * main_network:     Main DQN model.
        * target_network:   Target DQN model used for calculating target Q-values.
        * optimizer:        Optimizer used to train the model.
        * experience:       List of experience situations encountered across games.
        * possible_actions: Actions mapped with the output of the model.
    Out:
        * None.
    """

    # Ensure main_network is in train mode
    main_network.train()

    # Define the loss function
    loss_function = torch.nn.MSELoss()

    # Train loop
    total_loss = 0
    for b in range(NB_BATCHES):
        
        # Create a random batch
        batch_indices = random.sample(range(len(experience)), min(BATCH_SIZE, len(experience)))
        state_batch = torch.stack([experience[i]["state"] for i in batch_indices])
        action_batch = [experience[i]["action"] for i in batch_indices]
        reward_batch = torch.tensor([experience[i]["reward"] for i in batch_indices])
        next_state_batch = torch.stack([experience[i]["new_state"] for i in batch_indices])
        done_batch = torch.tensor([experience[i]["over"] for i in batch_indices])

        # Get current Q values from main network
        current_q_values = main_network(state_batch).gather(1, torch.tensor(action_batch).unsqueeze(1))

        # Compute next Q values from target network
        next_q_values = target_network(next_state_batch).max(1)[0].detach()
        expected_q_values = reward_batch + (DISCOUNT_FACTOR * next_q_values * (1 - done_batch))

        # Compute loss
        loss = loss_function(current_q_values.squeeze(1), expected_q_values)
        
        # Optimize the model
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        # Accumulate total loss for debugging
        total_loss += loss.item()

    # Optionally log the total loss
    if USE_WANDB:
        wandb.log({"total_loss": total_loss})

    
#####################################################################################################################################################
##################################################### EXECUTED ONCE AT THE BEGINNING OF THE GAME ####################################################
#####################################################################################################################################################

def preprocessing(maze, maze_width, maze_height, name, teams, player_locations, cheese, possible_actions, memory):
    """
    Cette fonction est appelée une fois au début du jeu pour effectuer des calculs complexes.
    """

    # Déterminez la hauteur (h) et la largeur (w) de l'état d'entrée pour le CNN
    h, w = maze_height, maze_width  # Remplacez par les dimensions appropriées
    outputs = len(possible_actions)  # Nombre d'actions possibles

    # Initialisation des réseaux Double DQN avec CNN
    memory.main_network = DQN(h, w, outputs)
    memory.target_network = DQN(h, w, outputs)
    memory.target_network.load_state_dict(memory.main_network.state_dict())

    # Initialisation de l'optimiseur
    memory.optimizer = torch.optim.SGD(memory.main_network.parameters(), lr=LEARNING_RATE)

    # Chargement des états des modèles et de l'optimiseur, si existants
    if os.path.exists(MODEL_FILE_NAME):
        memory.main_network.load_state_dict(torch.load(MODEL_FILE_NAME))
    if os.path.exists(OPTIMIZER_FILE_NAME):
        memory.optimizer.load_state_dict(torch.load(OPTIMIZER_FILE_NAME))

    # En mode d'entraînement, chargez l'expérience des jeux précédents
    memory.experience = []
    if TRAIN_MODEL:
        if os.path.exists(EXPERIENCE_FILE_NAME):
            memory.experience = torch.load(EXPERIENCE_FILE_NAME)

    memory.initial_cheese_count = len(cheese)
    memory.previous_scores = {p: 0 for p in player_locations}


#####################################################################################################################################################
######################################################### EXECUTED AT EACH TURN OF THE GAME #########################################################
#####################################################################################################################################################

def turn(maze, maze_width, maze_height, name, teams, player_locations, player_scores, player_muds, cheese, possible_actions, memory):
    


    """
    Cette fonction est appelée à chaque tour du jeu et doit renvoyer une action parmi l'ensemble des actions possibles.
    In:
        * maze, maze_width, maze_height, name, teams, player_locations, player_scores, player_muds, cheese, possible_actions: Informations sur l'état du jeu.
        * memory: Mémoire locale pour partager des informations entre preprocessing, turn et postprocessing.
    Out:
        * action: Une des actions possibles, comme indiqué dans possible_actions.
    """
    
    # Construire l'état actuel du jeu
    state = build_state(maze, maze_width, maze_height, name, teams, player_locations, cheese)

    # Copie des positions actuelles des joueurs
    old_player_locations = player_locations.copy()

    # Sélectionner une action et calculer la nouvelle position
    action = select_action(state, possible_actions, memory.main_network)
    new_player_position = calculate_new_position(player_locations[name], action, maze_width)

    # Mettre à jour les positions des joueurs
    new_player_locations = player_locations.copy()
    new_player_locations[name] = new_player_position

    # Logique d'entraînement du modèle
    if TRAIN_MODEL:
        # Supprimer les anciennes entrées d'expérience si nécessaire
        if len(memory.experience) >= MAX_EXPERIENCE_SIZE:
            del memory.experience[0]
        
        # Compléter le tour précédent et initialiser le courant
        if len(memory.experience) > 0 and "over" not in memory.experience[-1]:
            new_state = build_state(maze, maze_width, maze_height, name, teams, new_player_locations, cheese)
            memory.experience[-1]["reward"] = compute_reward(memory.experience[-1]["state"], memory.previous_scores, state, player_scores, name, teams, memory.initial_cheese_count)
            memory.experience[-1]["new_state"] = new_state
            memory.experience[-1]["over"] = False
        memory.experience.append({"state": state, "action": possible_actions.index(action)})

        # Sauvegarder les scores pour le prochain tour
        memory.previous_scores = player_scores

    # Incrémenter steps_done
    global steps_done
    steps_done += 1

    # Fin de la fonction
    return action



def calculate_new_position(current_position, action, maze_width):
    # Obtenez les coordonnées actuelles
    y, x = divmod(current_position, maze_width)

    # Mettez à jour les coordonnées en fonction de l'action
    if action == "up":
        y -= 1
    elif action == "down":
        y += 1
    elif action == "left":
        x -= 1
    elif action == "right":
        x += 1

    # Calculez la nouvelle position linéaire
    new_position = y * maze_width + x
    return new_position


#####################################################################################################################################################
######################################################## EXECUTED ONCE AT THE END OF THE GAME #######################################################
#####################################################################################################################################################

def postprocessing(maze, maze_width, maze_height, name, teams, player_locations, player_scores, player_muds, cheese, possible_actions, memory, stats):

    """
    This function is called once at the end of the game.
    It is not timed, and can be used to make some cleanup, analyses of the completed game, model training, etc.
    In:
        * maze, maze_width, maze_height, name, teams, player_locations, player_scores, player_muds, cheese, possible_actions: Game state information.
        * memory: Local memory to share information between preprocessing, turn, and postprocessing.
        * stats: Game statistics.
    Out:
        * None.
    """

    # Stuff to do if training the model
    if TRAIN_MODEL:

        # Complement the experience and save it
        state = build_state(maze, maze_width, maze_height, name, teams, player_locations, cheese)
        if len(memory.experience) > 0 and "over" not in memory.experience[-1]:
            new_state = build_state(maze, maze_width, maze_height, name, teams, player_locations, cheese)
            memory.experience[-1]["reward"] = compute_reward(memory.experience[-1]["state"], memory.previous_scores, state, player_scores, name, teams, memory.initial_cheese_count)

            memory.experience[-1]["new_state"] = new_state
            memory.experience[-1]["over"] = True
        torch.save(memory.experience, EXPERIENCE_FILE_NAME)

        # Train the model if we have enough memory
        if len(memory.experience) >= EXPERIENCE_MIN_SIZE_FOR_TRAINING:
            train_model(memory.main_network, memory.target_network, memory.optimizer, memory.experience, possible_actions)
            torch.save(memory.main_network.state_dict(), MODEL_FILE_NAME)

            # Update the target network
            update_target_network(memory.main_network, memory.target_network)

        # Log the final scores
        if USE_WANDB:
            opponent_name = get_opponent_name(teams, name)
            wandb.log({"final_score[player]": player_scores[name],
                       "final_score[opponent]": player_scores[opponent_name],
                       "final_score_difference": player_scores[name] - player_scores[opponent_name]})

#####################################################################################################################################################
######################################################################## GO! ########################################################################
#####################################################################################################################################################

if __name__ == "__main__":

    # Map the functions to the character
    players = [{"name": "RL",
                    "team": "You",
                    "skin": "rat",
                    "preprocessing_function": preprocessing,
                    "turn_function": turn,
                    "postprocessing_function": postprocessing},
               {"name": "Greedy",
                    "team": "Opponent",
                    "skin": "python",
                    "preprocessing_function": opponent.preprocessing if "preprocessing" in dir(opponent) else None,
                    "turn_function": opponent.turn,
                    "postprocessing_function": opponent.postprocessing if "postprocessing" in dir(opponent) else None}]

    # Customize the game elements
    config = {"maze_width": 21,
              "maze_height": 15,
              "cell_percentage": 100.0,
              "wall_percentage": 0.0,
              "mud_percentage": 0.0,
              "nb_cheese": 40}

    # Train mode
    if TRAIN_MODEL:

        # Remove old files if needed
        if RESET_TRAINING:
            if os.path.exists(OUTPUT_DIRECTORY):
                shutil.rmtree(OUTPUT_DIRECTORY, ignore_errors=True)
            os.mkdir(OUTPUT_DIRECTORY)
        
        # Connect to WandB for monitoring
        if USE_WANDB:
            wandb.login(key=open(WANDB_KEY_PATH).read().strip(), force=True)
            wandb.init(project="PyRat_RL", dir=OUTPUT_DIRECTORY)
        
        # Run multiple games with no GUI
        #config["synchronous"] = True
        config["game_mode"] = "sequential"
        config["preprocessing_time"] = 0.0
        config["turn_time"] = 0.0
        config["render_mode"] = "no_rendering"
        for i in tqdm.tqdm(range(NB_EPISODES), desc="Episode", position=0, leave=False):
            game = PyRat(players, **config)
            stats = game.start()
            if stats == {}:
                break
        steps_done += 1  # Augmenter steps_done après chaque partie

    # Test mode
    else:
        
        # Make a single game with GUI
        game = PyRat(players, **config)
        stats = game.start()
        print(stats)

#####################################################################################################################################################
#####################################################################################################################################################